// HelloWorld.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

// This function prints
void print()
{
    std::cout << "Hello, Skillbox!\n";
}

/*
* The questions on which comments shall answer:
* 1. What does it do?
* 2. Why does it do?
* 3. How does it do?
*/

int main()
{
    int x = 100;
    int y = x + 100;
    int mult = x * y;

    int random;     // this declaration
    random = 10;    // this assingnment
    float t1 = 0.1;  // this initialization

    std::cout << "Hello World!\n";
    print();

}